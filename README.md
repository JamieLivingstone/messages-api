# messages-api

A message board API that allows anonymous users to interact via persistent messages.

**NOTE: This project is based on my open-source [clean architecture template](https://github.com/JamieLivingstone/node-clean-architecture).**

## Trade-offs

I have made the following trade-offs in the interest of time, but I am aware of them.

- No deployment pipeline
- No architecture diagrams
- Documentation could be more comprehensive
- The domain model is [anemic](https://en.wikipedia.org/wiki/Anemic_domain_model) (but would likely grow with requirements)

## Getting Started

1. Install [NVM (Node version manager)](https://github.com/nvm-sh/nvm)
2. Install Docker and ensure that it is running
3. Install and set Node version `nvm install && nvm use`
4. Create .env file `cp .env.example .env`
5. Install project dependencies `yarn install`
6. Start container (local Postgres instance) `docker-compose up -d`
7. Migrate database `npx prisma migrate deploy`
8. Start development server `yarn start`
9. Navigate to OpenAPI documentation ([http://localhost:3000/api-docs](http://localhost:3000/api-docs))

## Scripts

**Build production bundle**

```
yarn build
```

**Lint project (eslint)**

```
yarn lint
```

**Start development server**

```
yarn start
```

**Run all tests**

```
yarn test
```

**Run unit tests**

```
yarn test:unit
```

**Run functional tests (API tests)**

```
yarn test:functional
```
