import { IRouter } from 'express';
import { Dependencies } from '@web/crosscutting/container';

export function messagesController({ dependencies, router }: { dependencies: Dependencies; router: IRouter }) {
  router.post('/api/v1/messages', async function createMessage(request, response, next) {
    try {
      const result = await dependencies.messages.commands.create(request.body);

      return response.status(201).json(result);
    } catch (error) {
      return next(error);
    }
  });

  router.delete('/api/v1/messages/:id', async function deleteMessage(request, response, next) {
    try {
      await dependencies.messages.commands.delete({ id: Number(request.params.id) });

      return response.status(200).json({});
    } catch (error) {
      return next(error);
    }
  });

  router.get('/api/v1/messages/:id', async function getMessage(request, response, next) {
    try {
      const result = await dependencies.messages.queries.get({ id: Number(request.params.id) });

      return response.status(200).json(result);
    } catch (error) {
      return next(error);
    }
  });

  router.get('/api/v1/messages', async function listMessages(request, response, next) {
    try {
      const result = await dependencies.messages.queries.list({
        pageNumber: Number(request.query.pageNumber ?? 1),
        pageSize: Number(request.query.pageSize ?? 10),
      });

      return response.status(200).json(result);
    } catch (error) {
      return next(error);
    }
  });

  router.patch('/api/v1/messages/:id', async function updateMessage(request, response, next) {
    try {
      await dependencies.messages.commands.update({
        id: Number(request.params.id),
        ...request.body,
      });

      return response.status(200).json({});
    } catch (error) {
      return next(error);
    }
  });

  return router;
}
