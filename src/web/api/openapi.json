{
  "openapi": "3.0.1",
  "info": {
    "title": "messages-api",
    "version": "v1"
  },
  "paths": {
    "/api/v1/messages": {
      "get": {
        "description": "List messages",
        "tags": ["Messages"],
        "parameters": [
          {
            "name": "pageNumber",
            "in": "query",
            "description": "The current page number",
            "required": true,
            "example": 1,
            "schema": {
              "type": "number"
            }
          },
          {
            "name": "pageSize",
            "in": "query",
            "description": "The number of messages per page",
            "required": true,
            "example": 10,
            "schema": {
              "type": "number"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Successfully retrieved messages",
            "content": {
              "application/json": {
                "schema": {
                  "properties": {
                    "count": {
                      "type": "number",
                      "example": "3"
                    },
                    "hasPreviousPage": {
                      "type": "boolean",
                      "example": false
                    },
                    "hasNextPage": {
                      "type": "boolean",
                      "example": true
                    },
                    "pageNumber": {
                      "type": "number",
                      "example": 1
                    },
                    "pageSize": {
                      "type": "number",
                      "example": 1
                    },
                    "messages": {
                      "type": "array",
                      "items": {
                        "type": "object",
                        "properties": {
                          "id": {
                            "type": "number",
                            "example": 1
                          },
                          "createdAt": {
                            "type": "string",
                            "example": "2022-10-23T12:01:39.748Z"
                          },
                          "updatedAt": {
                            "type": "string",
                            "example": "2022-10-23T12:01:39.748Z"
                          },
                          "content": {
                            "type": "string",
                            "example": "Example message content"
                          }
                        }
                      }
                    },
                    "totalPages": {
                      "type": "number",
                      "example": 3
                    }
                  }
                }
              }
            }
          },
          "400": {
            "description": "The request was invalid",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/BadRequestResponse"
                }
              }
            }
          }
        }
      },
      "post": {
        "description": "Create a new message",
        "tags": ["Messages"],
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "type": "object",
                "properties": {
                  "content": {
                    "type": "string",
                    "example": "Message content"
                  }
                },
                "required": ["content"]
              }
            }
          }
        },
        "responses": {
          "201": {
            "description": "Successfully created message",
            "content": {
              "application/json": {
                "schema": {
                  "properties": {
                    "id": {
                      "type": "number",
                      "example": "1"
                    }
                  }
                }
              }
            }
          },
          "400": {
            "description": "The request was invalid",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/BadRequestResponse"
                }
              }
            }
          }
        }
      }
    },
    "/api/v1/messages/{id}": {
      "delete": {
        "description": "Delete a message",
        "tags": ["Messages"],
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "The ID of the message to delete",
            "required": true,
            "schema": {
              "type": "number"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Successfully deleted message",
            "content": {
              "application/json": {
                "schema": {
                  "properties": {}
                }
              }
            }
          },
          "400": {
            "description": "The request was invalid",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/BadRequestResponse"
                }
              }
            }
          },
          "404": {
            "description": "The message does not exist",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/NotFoundResponse"
                }
              }
            }
          }
        }
      },
      "get": {
        "description": "Get a message",
        "tags": ["Messages"],
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "The ID of the message to retrieve",
            "required": true,
            "schema": {
              "type": "number"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Successfully retrieved message",
            "content": {
              "application/json": {
                "schema": {
                  "properties": {
                    "id": {
                      "type": "number",
                      "example": "1"
                    },
                    "createdAt": {
                      "type": "string",
                      "example": "2022-10-23T12:01:39.748Z"
                    },
                    "updatedAt": {
                      "type": "string",
                      "example": "2022-10-23T12:01:39.748Z"
                    },
                    "content": {
                      "type": "string",
                      "example": "Message content"
                    }
                  }
                }
              }
            }
          },
          "404": {
            "description": "The message does not exist",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/NotFoundResponse"
                }
              }
            }
          }
        }
      },
      "patch": {
        "description": "Update a message",
        "tags": ["Messages"],
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "The ID of the message to update",
            "required": true,
            "schema": {
              "type": "number"
            }
          }
        ],
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "type": "object",
                "properties": {
                  "content": {
                    "type": "string"
                  }
                }
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Successfully updated message",
            "content": {
              "application/json": {
                "schema": {
                  "properties": {}
                }
              }
            }
          },
          "400": {
            "description": "The request was invalid",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/BadRequestResponse"
                }
              }
            }
          },
          "404": {
            "description": "The message does not exist",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/NotFoundResponse"
                }
              }
            }
          }
        }
      }
    }
  },
  "components": {
    "schemas": {
      "ExceptionResponse": {
        "type": "object",
        "properties": {
          "detail": {
            "type": "string",
            "description": "A human-readable explanation specific to this occurrence of the problem"
          },
          "errors": {
            "type": "array",
            "description": "Exception details (e.g. validation result)",
            "items": {
              "type": "string"
            }
          },
          "status": {
            "type": "number",
            "description": "The HTTP status code"
          },
          "title": {
            "type": "string",
            "description": "A short, human-readable summary of the problem type"
          },
          "type": {
            "type": "string",
            "description": "A URI reference that identifies the problem type"
          }
        },
        "required": ["status", "title", "type"]
      },
      "BadRequestResponse": {
        "allOf": [
          {
            "$ref": "#/components/schemas/ExceptionResponse"
          }
        ],
        "example": {
          "status": 400,
          "title": "The request was invalid",
          "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1"
        }
      },
      "NotFoundResponse": {
        "allOf": [
          {
            "$ref": "#/components/schemas/ExceptionResponse"
          }
        ],
        "example": {
          "status": 404,
          "title": "The specified resource was not found",
          "type": "https://datatracker.ietf.org/doc/html/rfc7231#section-6.5.4"
        }
      },
      "InternalErrorResponse": {
        "allOf": [
          {
            "$ref": "#/components/schemas/ExceptionResponse"
          }
        ],
        "example": {
          "status": 500,
          "title": "An error occurred while processing your request",
          "type": "https://tools.ietf.org/html/rfc7231#section-6.6.1"
        }
      }
    }
  }
}
