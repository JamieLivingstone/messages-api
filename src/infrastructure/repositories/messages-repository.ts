import { Message as MessageModel } from '@prisma/client';
import { MessagesRepository } from '@application/common/interfaces';
import { Dependencies } from '@infrastructure/di';
import { Message } from '@domain/entities';

export function makeMessagesRepository({ db }: Pick<Dependencies, 'db'>): MessagesRepository {
  return {
    async create({ message }) {
      const { id } = await db.message.create({
        data: {
          createdAt: message.createdAt,
          updatedAt: message.updatedAt,
          content: message.content,
        },
      });

      return {
        id,
      };
    },
    async delete({ id }) {
      await db.message.delete({ where: { id } });
    },
    async getById({ id }) {
      const message = await db.message.findFirst({ where: { id } });

      if (!message) {
        return null;
      }

      return toEntity(message);
    },
    async list({ pageNumber, pageSize }) {
      const count = await db.message.count();

      const messages = await db.message.findMany({
        skip: (pageNumber - 1) * pageSize,
        take: pageSize,
      });

      return {
        count,
        messages: messages.map(toEntity),
      };
    },
    async update({ message }) {
      await db.message.update({
        where: {
          id: message.id,
        },
        data: {
          createdAt: message.createdAt,
          updatedAt: message.updatedAt,
          content: message.content,
        },
      });
    },
  };
}

function toEntity(message: MessageModel) {
  return new Message({
    id: message.id,
    createdAt: message.createdAt,
    updatedAt: message.updatedAt,
    content: message.content,
  });
}
