import { mockDeep } from 'jest-mock-extended';
import { PrismaClient, Message as MessageModel } from '@prisma/client';
import { makeMessagesRepository } from '../messages-repository';
import { Message } from '@domain/entities';

describe('messagesRepository', () => {
  function setup() {
    const messageModel: MessageModel = {
      id: 1,
      createdAt: new Date(2022, 10, 21),
      updatedAt: new Date(2022, 10, 21),
      content: 'Mock message',
    };

    const db = mockDeep<PrismaClient>({
      message: {
        count: jest.fn().mockResolvedValue(2),
        create: jest.fn().mockResolvedValueOnce(messageModel),
        findFirst: jest.fn().mockResolvedValue(messageModel),
        findMany: jest.fn().mockResolvedValue([messageModel, { ...messageModel, id: 2, content: 'Mock message two' }]),
      },
    });

    const messagesRepository = makeMessagesRepository({
      db,
    });

    return {
      db,
      messagesRepository,
      messageModel,
    };
  }

  describe('create', () => {
    it('should create message and return the generated id', async () => {
      const { db, messagesRepository, messageModel } = setup();

      const result = await messagesRepository.create({
        message: new Message({
          createdAt: messageModel.createdAt,
          updatedAt: messageModel.updatedAt,
          content: messageModel.content,
        }),
      });

      expect(result.id).toEqual(messageModel.id);
      expect(db.message.create).toHaveBeenCalledTimes(1);
      expect(db.message.create.mock.calls[0]).toMatchSnapshot();
    });
  });

  describe('delete', () => {
    it('should delete message', async () => {
      const { db, messagesRepository } = setup();

      await messagesRepository.delete({ id: 1 });

      expect(db.message.delete).toHaveBeenCalledTimes(1);
      expect(db.message.delete.mock.calls[0]).toMatchSnapshot();
    });
  });

  describe('getById', () => {
    describe('given the message does not exist', () => {
      it('should return null', async () => {
        const { db, messagesRepository } = setup();
        db.message.findFirst.mockResolvedValue(null);

        const message = await messagesRepository.getById({ id: 1 });

        expect(message).toBeNull();
        expect(db.message.findFirst).toHaveBeenCalledTimes(1);
        expect(db.message.findFirst.mock.calls[0]).toMatchSnapshot();
      });
    });

    describe('given the message exists', () => {
      it('should return the message', async () => {
        const { messagesRepository } = setup();

        const message = await messagesRepository.getById({ id: 1 });

        expect(message).toMatchSnapshot();
      });
    });
  });

  describe('list', () => {
    it('should return messages count and a paginated list', async () => {
      const { db, messagesRepository } = setup();

      const result = await messagesRepository.list({ pageNumber: 1, pageSize: 10 });

      expect(db.message.count).toBeCalledTimes(1);
      expect(db.message.findMany).toBeCalledTimes(1);
      expect(db.message.findMany.mock.calls[0]).toMatchSnapshot();
      expect(result).toMatchSnapshot();
    });
  });

  describe('update', () => {
    it('should update the specified message', async () => {
      const { db, messagesRepository, messageModel } = setup();

      await messagesRepository.update({
        message: new Message({
          createdAt: messageModel.createdAt,
          updatedAt: messageModel.updatedAt,
          content: messageModel.content,
        }),
      });

      expect(db.message.update).toHaveBeenCalledTimes(1);
      expect(db.message.update.mock.calls[0]).toMatchSnapshot();
    });
  });
});
