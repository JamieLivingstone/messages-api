import winston from 'winston';
import { Logger } from '@application/common/interfaces';

export function makeLogger(): Logger {
  return winston.createLogger({
    format: winston.format.json(),
    level: 'info',
    transports: [
      new winston.transports.Console({
        format: winston.format.json(),
        level: 'info',
        silent: process.env.NODE_ENV === 'test',
      }),
    ],
  });
}
