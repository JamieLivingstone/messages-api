export class Message {
  public readonly id?: number;
  public readonly createdAt: Date;
  public updatedAt: Date;
  public content: string;

  constructor(message: { id?: number; createdAt: Date; updatedAt: Date; content: string }) {
    this.id = message.id;
    this.createdAt = message.createdAt;
    this.updatedAt = message.updatedAt;
    this.content = message.content;
  }
}
