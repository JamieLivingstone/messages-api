import { asFunction, Resolver } from 'awilix';
import { makeMessages } from './messages';

export type Dependencies = {
  messages: ReturnType<typeof makeMessages>;
};

export function makeApplication(): { [dependency in keyof Dependencies]: Resolver<Dependencies[dependency]> } {
  return {
    messages: asFunction(makeMessages).singleton(),
  };
}
