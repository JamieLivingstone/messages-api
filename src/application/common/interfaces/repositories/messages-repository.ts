import { Message } from '@domain/entities';

export interface MessagesRepository {
  create(parameters: { message: Message }): Promise<{ id: number }>;
  delete(parameters: { id: number }): Promise<void>;
  getById(parameters: { id: number }): Promise<Message | null>;
  list(parameters: { pageNumber: number; pageSize: number }): Promise<{ count: number; messages: Array<Message> }>;
  update(parameters: { message: Message }): Promise<void>;
}
