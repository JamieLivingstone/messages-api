import { Dependencies } from '@infrastructure/di';
import { NotFoundException } from '@application/common/exceptions';
import { validate } from './delete-message-command-validator';

export type DeleteMessageCommand = Readonly<{
  id: number;
}>;

export function makeDeleteMessageCommand({ messagesRepository }: Pick<Dependencies, 'messagesRepository'>) {
  return async function deleteMessageCommand(command: DeleteMessageCommand) {
    await validate(command);

    const { id } = command;

    const message = await messagesRepository.getById({ id });

    if (!message) {
      throw new NotFoundException(`Message ${id} does does not exist`);
    }

    await messagesRepository.delete({ id });
  };
}
