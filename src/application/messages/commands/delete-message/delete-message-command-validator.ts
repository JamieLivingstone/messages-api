import * as Yup from 'yup';
import { ValidationException } from '@application/common/exceptions';
import { DeleteMessageCommand } from './delete-message-command';

export async function validate(command: DeleteMessageCommand) {
  try {
    const schema: Yup.SchemaOf<DeleteMessageCommand> = Yup.object().shape({
      id: Yup.number().positive().required(),
    });

    await schema.validate(command, { abortEarly: false, strict: true });
  } catch (error) {
    throw new ValidationException(error as Yup.ValidationError);
  }
}
