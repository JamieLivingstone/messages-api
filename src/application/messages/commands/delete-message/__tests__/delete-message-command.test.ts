import { mockDeep } from 'jest-mock-extended';
import { MessagesRepository } from '@application/common/interfaces';
import { ValidationException, NotFoundException } from '@application/common/exceptions';
import { Message } from '@domain/entities';
import { makeDeleteMessageCommand } from '../delete-message-command';

describe('deleteMessageCommand', () => {
  function setup() {
    const messagesRepository = mockDeep<MessagesRepository>({
      getById: jest.fn().mockResolvedValue(
        new Message({
          id: 1,
          createdAt: new Date(2022, 1, 1),
          updatedAt: new Date(2022, 1, 1),
          content: 'Message one',
        }),
      ),
      delete: jest.fn().mockResolvedValue({}),
    });

    const deleteMessageCommand = makeDeleteMessageCommand({
      messagesRepository,
    });

    return {
      messagesRepository,
      deleteMessageCommand,
    };
  }

  describe('given an invalid command', () => {
    it('should throw validation exception', async () => {
      const { deleteMessageCommand } = setup();

      const result = deleteMessageCommand({
        id: 0, // Must be a positive number
      });

      await expect(result).rejects.toThrow(ValidationException);
    });
  });

  describe('given the message does not exist', () => {
    it('should throw not found exception', async () => {
      const { messagesRepository, deleteMessageCommand } = setup();
      messagesRepository.getById.mockResolvedValueOnce(null);

      const result = deleteMessageCommand({ id: 1 });

      await expect(result).rejects.toThrow(NotFoundException);
    });
  });

  describe('given a valid command and the message exists', () => {
    it('should delete message', async () => {
      const { messagesRepository, deleteMessageCommand } = setup();

      await deleteMessageCommand({ id: 1 });

      expect(messagesRepository.delete).toHaveBeenCalledTimes(1);
    });
  });
});
