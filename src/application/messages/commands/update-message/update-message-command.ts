import { Dependencies } from '@infrastructure/di';
import { NotFoundException } from '@application/common/exceptions';
import { validate } from './update-message-command-validator';

export type UpdateMessageCommand = Readonly<{
  id: number;
  content?: string;
}>;

export function makeUpdateMessageCommand({ messagesRepository }: Pick<Dependencies, 'messagesRepository'>) {
  return async function updateMessageCommand(command: UpdateMessageCommand) {
    await validate(command);

    const { id, content } = command;

    const message = await messagesRepository.getById({ id });

    if (!message) {
      throw new NotFoundException(`Message ${id} does does not exist`);
    }

    message.updatedAt = new Date();

    if (typeof content !== 'undefined') {
      message.content = content;
    }

    await messagesRepository.update({ message });
  };
}
