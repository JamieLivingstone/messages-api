import { mockDeep } from 'jest-mock-extended';
import { MessagesRepository } from '@application/common/interfaces';
import { ValidationException, NotFoundException } from '@application/common/exceptions';
import { Message } from '@domain/entities';
import { makeUpdateMessageCommand } from '../update-message-command';

describe('updateMessageCommand', () => {
  function setup() {
    const messagesRepository = mockDeep<MessagesRepository>({
      getById: jest.fn().mockResolvedValue(
        new Message({
          id: 1,
          createdAt: new Date(2022, 1, 1),
          updatedAt: new Date(2022, 1, 1),
          content: 'Message one',
        }),
      ),
      update: jest.fn().mockResolvedValue({}),
    });

    const updateMessageCommand = makeUpdateMessageCommand({
      messagesRepository,
    });

    return {
      messagesRepository,
      updateMessageCommand,
    };
  }

  describe('given an invalid command', () => {
    it('should throw validation exception', async () => {
      const { updateMessageCommand } = setup();

      const result = updateMessageCommand({
        id: 0, // Must be a positive number
      });

      await expect(result).rejects.toThrow(ValidationException);
    });
  });

  describe('given the message does not exist', () => {
    it('should throw not found exception', async () => {
      const { messagesRepository, updateMessageCommand } = setup();
      messagesRepository.getById.mockResolvedValueOnce(null);

      const result = updateMessageCommand({ id: 1 });

      await expect(result).rejects.toThrow(NotFoundException);
    });
  });

  describe('given a valid command and the message exists', () => {
    it('should update message', async () => {
      const { messagesRepository, updateMessageCommand } = setup();

      await updateMessageCommand({
        id: 1,
        content: 'Updated content',
      });

      expect(messagesRepository.update).toHaveBeenCalledTimes(1);
    });
  });
});
