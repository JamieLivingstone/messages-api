import * as Yup from 'yup';
import { ValidationException } from '@application/common/exceptions';
import { UpdateMessageCommand } from './update-message-command';

export async function validate(command: UpdateMessageCommand) {
  try {
    const schema: Yup.SchemaOf<UpdateMessageCommand> = Yup.object().shape({
      id: Yup.number().positive().required(),
      content: Yup.string().min(1).optional(),
    });

    await schema.validate(command, { abortEarly: false, strict: true });
  } catch (error) {
    throw new ValidationException(error as Yup.ValidationError);
  }
}
