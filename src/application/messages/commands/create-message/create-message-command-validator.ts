import * as Yup from 'yup';
import { ValidationException } from '@application/common/exceptions';
import { CreateMessageCommand } from './create-message-command';

export async function validate(command: CreateMessageCommand) {
  try {
    const schema: Yup.SchemaOf<CreateMessageCommand> = Yup.object().shape({
      content: Yup.string().min(1).required(),
    });

    await schema.validate(command, { abortEarly: false, strict: true });
  } catch (error) {
    throw new ValidationException(error as Yup.ValidationError);
  }
}
