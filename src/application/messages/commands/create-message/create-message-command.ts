import { Dependencies } from '@infrastructure/di';
import { Message } from '@domain/entities';
import { validate } from './create-message-command-validator';

export type CreateMessageCommand = Readonly<{
  content: string;
}>;

export function makeCreateMessageCommand({ messagesRepository }: Pick<Dependencies, 'messagesRepository'>) {
  return async function createMessageCommand(command: CreateMessageCommand) {
    await validate(command);

    const now = new Date();

    const message = new Message({
      createdAt: now,
      updatedAt: now,
      content: command.content,
    });

    const { id } = await messagesRepository.create({ message });

    return {
      id,
    };
  };
}
