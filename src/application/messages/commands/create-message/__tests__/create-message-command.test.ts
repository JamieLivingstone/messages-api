import { mockDeep } from 'jest-mock-extended';
import { MessagesRepository } from '@application/common/interfaces';
import { ValidationException } from '@application/common/exceptions';
import { makeCreateMessageCommand } from '../create-message-command';

describe('createMessageCommand', () => {
  function setup() {
    const messagesRepository = mockDeep<MessagesRepository>({
      create: jest.fn().mockResolvedValue({ id: 1 }),
    });

    const createMessageCommand = makeCreateMessageCommand({
      messagesRepository,
    });

    return {
      messagesRepository,
      createMessageCommand,
    };
  }

  describe('given an invalid command', () => {
    it('should throw validation exception', async () => {
      const { createMessageCommand } = setup();

      const result = createMessageCommand({
        content: '', // Cannot be empty
      });

      await expect(result).rejects.toThrow(ValidationException);
    });
  });

  describe('given a valid command', () => {
    it('should create message', async () => {
      const { createMessageCommand, messagesRepository } = setup();

      const result = await createMessageCommand({
        content: 'Hello world!',
      });

      expect(result).toEqual({ id: 1 });
      expect(messagesRepository.create).toHaveBeenCalledTimes(1);
    });
  });
});
