import { Dependencies } from '@infrastructure/di';
import { NotFoundException } from '@application/common/exceptions';
import { toDto } from './get-message-query-mapper';

export type GetMessageQuery = Readonly<{
  id: number;
}>;

export function makeGetMessageQuery({ messagesRepository }: Pick<Dependencies, 'messagesRepository'>) {
  return async function getMessageQuery({ id }: GetMessageQuery) {
    const message = await messagesRepository.getById({ id });

    if (!message) {
      throw new NotFoundException(`Message ${id} does does not exist`);
    }

    return toDto(message);
  };
}
