import { Message } from '@domain/entities';

export function toDto(message: Message) {
  return {
    id: message.id,
    createdAt: message.createdAt,
    updatedAt: message.updatedAt,
    content: message.content,
  };
}
