import { mockDeep } from 'jest-mock-extended';
import { MessagesRepository } from '@application/common/interfaces';
import { NotFoundException } from '@application/common/exceptions';
import { Message } from '@domain/entities';
import { makeGetMessageQuery } from '../get-message-query';

describe('getMessageQuery', () => {
  function setup() {
    const messagesRepository = mockDeep<MessagesRepository>({
      getById: jest.fn().mockResolvedValue(
        new Message({
          id: 1,
          createdAt: new Date(2022, 1, 1),
          updatedAt: new Date(2022, 1, 1),
          content: 'Message one',
        }),
      ),
      update: jest.fn().mockResolvedValue({}),
    });

    const getMessageQuery = makeGetMessageQuery({
      messagesRepository,
    });

    return {
      messagesRepository,
      getMessageQuery,
    };
  }

  describe('given the message does not exist', () => {
    it('should throw not found exception', async () => {
      const { messagesRepository, getMessageQuery } = setup();
      messagesRepository.getById.mockResolvedValueOnce(null);

      const result = getMessageQuery({ id: 1 });

      await expect(result).rejects.toThrow(NotFoundException);
    });
  });

  describe('given the message exists', () => {
    it('should retrieve message and map to a domain transfer object', async () => {
      const { messagesRepository, getMessageQuery } = setup();

      const result = await getMessageQuery({ id: 1 });

      expect(messagesRepository.getById).toHaveBeenCalledTimes(1);
      expect(result).toMatchSnapshot();
    });
  });
});
