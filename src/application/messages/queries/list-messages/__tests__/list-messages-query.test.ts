import { mockDeep } from 'jest-mock-extended';
import { MessagesRepository } from '@application/common/interfaces';
import { ValidationException } from '@application/common/exceptions';
import { Message } from '@domain/entities';
import { makeListMessagesQuery } from '../list-messages-query';

describe('listMessagesQuery', () => {
  function setup() {
    const messagesRepository = mockDeep<MessagesRepository>();
    const listMessagesQuery = makeListMessagesQuery({ messagesRepository });

    return {
      listMessagesQuery,
      messagesRepository,
    };
  }

  describe('given an invalid query', () => {
    it('should throw validation exception', async () => {
      const { listMessagesQuery } = setup();

      const result = listMessagesQuery({
        pageNumber: 1,
        pageSize: 1000, // Cannot exceed 50
      });

      await expect(result).rejects.toThrow(ValidationException);
    });
  });

  describe('given a valid query', () => {
    it('should retrieve message and map to a paginated domain transfer object', async () => {
      const { messagesRepository, listMessagesQuery } = setup();

      messagesRepository.list.mockResolvedValueOnce({
        count: 11,
        messages: [
          new Message({
            id: 11,
            createdAt: new Date(2022, 1, 1),
            updatedAt: new Date(2022, 1, 1),
            content: 'Message eleven',
          }),
        ],
      });

      const result = await listMessagesQuery({
        pageNumber: 2,
        pageSize: 10,
      });

      expect(messagesRepository.list).toHaveBeenCalledTimes(1);
      expect(result).toMatchSnapshot();
    });
  });
});
