import { Dependencies } from '@infrastructure/di';
import { toDto } from './list-messages-query-mapper';
import { validate } from './list-messages-query-validator';

export type ListMessagesQuery = Readonly<{
  pageNumber: number;
  pageSize: number;
}>;

export function makeListMessagesQuery({ messagesRepository }: Pick<Dependencies, 'messagesRepository'>) {
  return async function listMessagesQuery(query: ListMessagesQuery) {
    await validate(query);

    const { pageNumber, pageSize } = query;

    const { count, messages } = await messagesRepository.list({ pageNumber, pageSize });

    return toDto({
      count,
      pageNumber,
      pageSize,
      messages,
    });
  };
}
