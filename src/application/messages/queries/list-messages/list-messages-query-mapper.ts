import { Message } from '@domain/entities';

export function toDto({
  count,
  pageNumber,
  pageSize,
  messages,
}: {
  count: number;
  pageNumber: number;
  pageSize: number;
  messages: Array<Message>;
}) {
  const totalPages = Math.ceil(count / pageSize);

  return {
    count,
    hasPreviousPage: pageNumber > 1,
    hasNextPage: pageNumber < totalPages,
    pageNumber,
    pageSize,
    messages: messages.map((message) => ({
      id: message.id,
      createdAt: message.createdAt,
      updatedAt: message.updatedAt,
      content: message.content,
    })),
    totalPages,
  };
}
