import { Dependencies } from '@infrastructure/di';
import { makeCreateMessageCommand } from './commands/create-message';
import { makeDeleteMessageCommand } from './commands/delete-message';
import { makeUpdateMessageCommand } from './commands/update-message';
import { makeGetMessageQuery } from './queries/get-message';
import { makeListMessagesQuery } from './queries/list-messages';

export function makeMessages(dependencies: Dependencies) {
  return {
    commands: {
      create: makeCreateMessageCommand(dependencies),
      delete: makeDeleteMessageCommand(dependencies),
      update: makeUpdateMessageCommand(dependencies),
    },
    queries: {
      get: makeGetMessageQuery(dependencies),
      list: makeListMessagesQuery(dependencies),
    },
  };
}
