import { Message } from '../../src/domain/entities';
import { PrismaClient } from '@prisma/client';

export async function initializeDbForTests() {
  const client = new PrismaClient();

  await Promise.all([
    client.message.createMany({
      data: messages.map((message) => ({
        createdAt: message.createdAt,
        updatedAt: message.updatedAt,
        content: message.content,
      })),
    }),
  ]);
}

export const messages: Array<Message> = [
  new Message({
    id: 1,
    createdAt: new Date(2022, 1, 1),
    updatedAt: new Date(2022, 1, 2),
    content: 'Message one',
  }),
  new Message({
    id: 2,
    createdAt: new Date(2022, 2, 1),
    updatedAt: new Date(2022, 3, 2),
    content: 'Message two',
  }),
  new Message({
    id: 3,
    createdAt: new Date(2022, 4, 1),
    updatedAt: new Date(2022, 5, 2),
    content: 'Message three',
  }),
  new Message({
    id: 4,
    createdAt: new Date(2022, 5, 1),
    updatedAt: new Date(2022, 6, 2),
    content: 'Message four',
  }),
  new Message({
    id: 5,
    createdAt: new Date(2022, 7, 1),
    updatedAt: new Date(2022, 7, 2),
    content: 'Message five',
  }),
  new Message({
    id: 6,
    createdAt: new Date(2022, 8, 1),
    updatedAt: new Date(2022, 8, 2),
    content: 'Message six',
  }),
];
