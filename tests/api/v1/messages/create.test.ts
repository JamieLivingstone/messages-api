import { client } from '../..';

describe('POST /api/v1/messages', () => {
  describe('given an invalid request', () => {
    it('should respond with a 400 status code', async () => {
      const response = await client.post('/api/v1/messages').send({
        content: '', // Cannot be empty
      });

      expect(response.status).toEqual(400);
    });
  });

  describe('given a valid request', () => {
    it('should respond with a 201 status code', async () => {
      const response = await client.post('/api/v1/messages').send({
        content: 'Example message',
      });

      expect(response.status).toEqual(201);
      expect(response.body).toMatchSnapshot({ id: expect.any(Number) });
    });
  });
});
