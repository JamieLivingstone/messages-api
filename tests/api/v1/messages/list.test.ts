import { client } from '../..';

describe('GET /api/v1/messages', () => {
  describe('given an invalid request', () => {
    it('should respond with a 400 status code', async () => {
      const pageSize = 1000; // Cannot exceed 50

      const response = await client.get(`/api/v1/messages?pageSize=${pageSize}`);

      expect(response.status).toEqual(400);
    });
  });

  describe('given a valid request', () => {
    it('should respond with a 200 status code', async () => {
      const pageNumber = 3;
      const pageSize = 2;

      const response = await client.get(`/api/v1/messages?pageNumber=${pageNumber}&pageSize=${pageSize}`);

      expect(response.status).toEqual(200);
      expect(response.body).toMatchSnapshot();
    });
  });
});
