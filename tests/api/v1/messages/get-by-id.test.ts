import { client, seed } from '../..';

describe('GET /api/v1/messages/:id', () => {
  describe('given the message does not exist', () => {
    it('should respond with a 404 status code', async () => {
      const messageId = Number.MAX_SAFE_INTEGER;

      const response = await client.get(`/api/v1/messages/${messageId}`);

      expect(response.status).toEqual(404);
    });
  });

  describe('given a valid request', () => {
    it('should respond with a 200 status code', async () => {
      const messageId = seed.messages[0].id;

      const response = await client.get(`/api/v1/messages/${messageId}`);

      expect(response.status).toEqual(200);
      expect(response.body).toMatchSnapshot();
    });
  });
});
