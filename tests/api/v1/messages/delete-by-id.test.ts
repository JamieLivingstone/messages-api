import { client, seed } from '../..';

describe('DELETE /api/v1/messages/:id', () => {
  describe('given an invalid request', () => {
    it('should respond with a 400 status code', async () => {
      const messageId = 'invalid';

      const response = await client.delete(`/api/v1/messages/${messageId}`);

      expect(response.status).toEqual(400);
    });
  });

  describe('given the message does not exist', () => {
    it('should respond with a 404 status code', async () => {
      const messageId = Number.MAX_SAFE_INTEGER;

      const response = await client.delete(`/api/v1/messages/${messageId}`);

      expect(response.status).toEqual(404);
    });
  });

  describe('given a valid request', () => {
    it('should respond with a 200 status code', async () => {
      const messageId = seed.messages[0].id;

      const response = await client.delete(`/api/v1/messages/${messageId}`);

      expect(response.status).toEqual(200);
    });
  });
});
